# Zen Reading mode for Haaretz

A chrome extension to automatically enable Zen Reading mode on the Hebrew website of Haaretz and TheMarker.

The extension is available [here](https://chrome.google.com/webstore/detail/%D7%A7%D7%A8%D7%99%D7%90%D7%AA-%D7%96%D7%9F-%D7%91%D7%A2%D7%99%D7%AA%D7%95%D7%9F-%D7%94%D7%90%D7%A8%D7%A5/dfdmkmeffgpfnnbolblceokkclkacmma).
